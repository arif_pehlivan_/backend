
const pool=require('../database/keys');
const authentication = {};
const bcrypt = require("bcryptjs");

authentication.signUp = async (req, res) => {
  const { first_name,last_name, email, password, role } = req.body;
  const salt = bcrypt.genSaltSync(10);
  const b_password = bcrypt.hashSync(password,salt)
  console.log(b_password);
  if (role == "doctor") {
    try {
      await pool.query(
        "INSERT INTO doctors (first_name, last_name, email, password) VALUES ($1, $2, $3, $4)",
        [first_name,last_name, email, b_password]
      );
      res.status(200).json({
        message: "Successful registred admin",
        admin: { first_name,last_name, email, password },
      });
    } catch (error) {
      if (error.constraint == "doctor_key") {
        res.status(500).json({
          message: "Someone is already using that email",
          error,
        });
      } else {
        res.status(500).json({
          message: "An error has ocurred",
          error,
        });
      }
    }
  } else {
   
    try {
      await pool.query(
        "INSERT INTO users (first_name,last_name,email,password) VALUES ($1, $2, $3,$4)",
        [first_name, last_name,email,password]
      );
      res.status(200).json({
        message: "Successful registred users",
        users: { first_name,last_name, email },
      });
    } catch (error) {
      if (error.constraint == "user_key") {
        res.status(500).json({
          message: "Someone is already using that email",
          error,
        });
      } else {
        res.status(500).json({
          message: "An error has ocurred",
          error,
        });
        console.log(error);
      }
    }
  }
};

authentication.signIn = async (req, res) => {
  const { email, b_password, role } = req.body;
  // const salt = bcrypt.genSaltSync(10);
  // const password = bcrypt.hashSync(b_password,salt);

  
 
  if (role == "doctor") {
    try {
      const doctor = await (
        await pool.query(
          "SELECT * FROM doctors WHERE email=$1 AND password=$2",
          [email, b_password]
        )
      ).rows;
     const  b1_password= await  bcrypt.compare(b_password,doctor[0].password);
     console.log(b1_password);
      console.log(doctor[0].password);
    //  console.log(b_password);
   
      if (doctor.length > 0) {
        res.status(200).json({
          id: doctor[0].id,
          name: doctor[0].user_name,
          email: doctor[0].email,
          role: "doctor"
        });
      } else {
        res.status(200).json({
          message: "The doctor does not exist",
          NotFound: true,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "An error has ocured",
        error,
      });
    }
  } else {
    try {
      const users = await (
        await pool.query(
          "SELECT * FROM users WHERE email=$1 AND password=$2",
          [email, password]
        )
      ).rows;
      if (users.length > 0) {
        res.status(200).json({
          id: users[0].id_s,
          name: users[0].s_name,
          email: users[0].s_email,
          role: "user",
        });
      } else {
        res.status(200).json({
          message: "Users does not exist",
          NotFound: true,
        });
      }
    } catch (error) {
      res.status(500).json({
        message: "An error has ocured",
        error,
      });
    }
  }
};

module.exports = authentication;
